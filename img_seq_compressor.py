## Image sequences compressor v0.2.0
## ready to bundle with IM's compare.exe using pyinstaller with following command:
## pyinstaller.exe --onefile --icon=app.ico --add-data "compare.exe;." img_seq_compressor.py 
#
## Python script with PySide2 GUI to handle image redundancy using image magick
## Allow to compress / fill / expand image sequences

# MIT License
# 
# Copyright (c) 2020 Samuel Bernou
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#---------------

## TODO
# - Feature : add support for no-range named sequence
# - Cleaner Batch launcher : use function taking single func for batch launcher

import sys
import os, re, shutil
import subprocess

#### --- /module auto install

def module_can_be_imported(name):
    try:
        __import__(name)
        return True
    except ModuleNotFoundError:
        return False

def install_package(name):
    subprocess.run(['python', "-m", "pip", "install", name])


def setup(dependencies):
    '''
    Get a set containing multiple tuple of (module, package) names pair (str)
    install pip with ensurepip if needed, try to import, install, retry
    '''

    # if not module_can_be_imported("pip"):
    #     subprocess.run(['python', "-m", "ensurepip"])

    for module_name, package_name in dependencies:
        if not module_can_be_imported(module_name):
            print(f"Installing module: {module_name} - package: {package_name}")
            install_package(package_name)
            module_can_be_imported(package_name)


# name (used for import), package_name (used for pip install)
dependencies = [
    ('PySide2','PySide2'),
]

if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    pass#print('running in a PyInstaller bundle')
else:
    setup(dependencies) # bundled, no need for dependancies

#### --- module auto install/

from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtCore import QCoreApplication

## ------
#  CODE
## ------

from os.path import splitext, join, abspath, exists, isdir, isfile, dirname, basename
from time import time


### ---------
##  images comparison utilities
### ---------


if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    # print('sys._MEIPASS: ', sys._MEIPASS)
    # print('sys.path[0]: ', sys.path[0])# base...pack.zip
    compare = os.path.join(sys._MEIPASS, "compare.exe")
    # print('sys.executable: ', sys.executable)
    # print('os.path.exists(compare): ', os.path.exists(compare))

else:
    # running in a normal Python process
    compare = 'compare'# point to IMagick compare binary


image_exts = ('.png', '.jpg', '.tiff', '.tga', '.jpeg',)
exception_list = ('thumbs.db',)

def imagick_comp_RSME(fp1, fp2, out = ''):
    ''' Metric Root Mean Square Error (RMSE)
    Use image magick to get a difference percentage between two image using metric
    doc : https://imagemagick.org/script/command-line-options.php#metric
    application : https://stackoverflow.com/questions/7940935/equality-test-of-images-using-imagemagick
    '''
    import subprocess
    out = out if out else 'NULL:'#redirect to null
    ## Calculate Root Mean Square Error (RMSE) to see how much pixels tend to differ
    result = subprocess.getoutput([compare, '-metric', 'RMSE', fp1, fp2, out])
    result = result.split()[-1].strip(' ()')
    result = float(result)*100# convert to percentage of diff (result is 0 to 1)

    # if result < 0.001: # less than this percentage of global diff is considered similar.# maybe add it as a tolerance variable
    #     if result > 0:
    #         print(f'Difference (under tolerance percentage) : {result}% : {basename(fp1)} <-> {basename(fp2)}')
    #     return 0
    return result

def imagick_comp_AE(fp1, fp2, diff_percent=0.5, out = ''):
    ''' Metric absolute pixel count
    Use image magick to get a difference percentage between two image using metric
    doc : https://imagemagick.org/script/command-line-options.php#metric
    application : https://stackoverflow.com/questions/7940935/equality-test-of-images-using-imagemagick
    '''
    import subprocess
    out = out if out else 'NULL:'#redirect to null

    ## Pixels may only differ by 0.5% before being considered different (can be used as tolerance)
    result = subprocess.getoutput([compare, '-metric', 'AE', '-fuzz', f'{diff_percent:.2f}%', fp1, fp2, out])
    # print(f'diff {basename(fp1)}-> {basename(fp2)}: {result}')
    return int(result)

###--- compressor

def compress_sequence(fp, diff_percent=0.06, rename=True, dryrun=False):
    '''
    Delete redundant images
    if rename is True: write the range in name of the first img of each series
    else name stay as is and last frame is never deleted (redundant or not) to know where sequence stop.
    ex: Keep only frame 4 on identical sequence 4,5,6,7,8 and rename img_0004-0008 (fix from image 4 to 8)
    '''
    imglist = [f for f in os.listdir(fp) if splitext(f)[1].lower() in image_exts and not f in exception_list]
    imglist.sort()
    
    if not imglist or len(imglist) <= 1:
        report = f'No image sequence to scan : {fp}'
        print(report)
        return report

    start = time()
    # check if not an alredy compressed sequence (check for \d{2,5}-\d{2,5}\.?)
    for i in imglist:
        if re.search(r'\d{2,5}-\d{2,5}\.?', i):
            report = f'Image {i} is already on compressed format (range numerotation) : {fp}'
            print(report)
            return report
            
    # Check if sequence is valid
    base = re.split(r'\d+(?!.*\d)', imglist[0])[0] #split on last number and get first part
    if not base:
        report = f'Could not found number in image name: {imglist[0]} : {fp}'
        print(report)
        return report

    unmatched = [i for i in imglist if not i.startswith(base)]
    if unmatched:
        report = f'{len(unmatched)} images not starting by "{base}" : {unmatched[:3]} : {fp}'
        print(report)
        return report

    todel = []
    frame_list = [join(fp, imglist[0])]#need the first image
    stop_nums = []
    viewed = 0

    print(f' +    {splitext(imglist[0])[0]}')
    for i, f in enumerate(imglist):
        pic = join(fp,f)
        if viewed:
            diff = imagick_comp_RSME(prev_pic, pic)
            # diff = imagick_comp_AE(prev_pic, pic, diff_percent) # diff (> 0)
            if diff > diff_percent:
                print(f' +    {splitext(basename(pic))[0]} {diff:.3f}') # (ae:{imagick_comp_AE(prev_pic, pic, diff_percent=1)})
                frame_list.append(pic)
                stop_nums.append(re.search(r'\d+(?!.*\d)', basename(prev_pic)).group(0))
            else:# same (= 0)
                print(f'- {splitext(basename(pic))[0]} {diff:.3f}') # (ae:{imagick_comp_AE(prev_pic, pic, diff_percent=1)})
                todel.append(pic)

        prev_pic = pic
        viewed = 1

    # Without renaming must keep last frame if it was redundant one to delete
    if not rename and basename(todel[-1]) == imglist[-1]:
        frame_list.append(todel.pop())

    #add last number
    stop_nums.append(re.search(r'\d+(?!.*\d)', imglist[-1]).group(0))

    # print(f'frame keep list ({len(frame_list)}) frame stops list ({len(stop_nums)})')
    if len(frame_list) != len(stop_nums):
        report = f'Error: frame to keep ({len(frame_list)}) has no equal frame stops ({len(stop_nums)}) : {fp}'
        print(report)
        return report 

    print('Deleting duplicate...', end=' ')#, flush=True
    for im in todel:
        # print(f'remove {im}')
        if not dryrun: os.remove(im)
    print('Done')

    ## add range if renaming is on
    if rename:
        print('Renaming...', end=' ')#, flush=True
        for im, stopnum in zip(frame_list, stop_nums):
            root, ext = splitext(im)
            new = f'{root}-{stopnum}{ext}'
            print('new: ', new)
            if not dryrun: os.rename(im, new)
        print('Done')

    result = f'Compress: {len(todel)}/{len(imglist)} imgs deleted (kept: {len(frame_list)}) in {time()-start:.2f}s at : {fp}'
    print(result)
    return result


### ---------
##  Checker
### ---------

def sequence_gap_check(fp) -> int:
    '''
    Check if there is gap in sequence numerotation in folder passed.
    Make comprehensive prints
    return number of gaps
    '''

    l = [f for f in os.listdir(fp) if splitext(f)[1].lower() in image_exts and not f in exception_list]
    l.sort()
    miss = 0
    gap = 0
    i = 0
    #to update : compile regex and search for the rightmost number (might work as is)
    for f in l:
        num = re.search(r'^(.*?)(\d+)(\D*)$', l[i])
        num = int(num.group(2))
        if i:
            prev = re.search(r'^(.*?)(\d+)(\D*)$', l[i-1])
            prev = int(prev.group(2))
            res = num - prev
            if res > 1:
                gaplength = res - 1
                miss += gaplength
                gap += 1
                if res == 2:
                    print (f"gap {num - 1}")#gaplength
                else:
                    print (f"gap {prev + 1} - {num - 1} : {gaplength} files")
        i += 1

    if gap:
        print (gap, 'gap found')
        print (miss, "total missing")
    print("finished :", i, "scanned")
    return gap

### ---------
##  Filler
### ---------

def expand_sequence(fp):
    '''
    Fill gaps from compressed sequences with range in file names
    Duplicate images according to range and rename to reconstruct sequence with all images
    ex: img_0004-0008 is duplicated and renamed to recreate the sequence img_0004, img_0005...
    '''
    import shutil
    no_num = [f for f in os.listdir(fp) if splitext(f)[1].lower() in image_exts and not f in exception_list and not re.search(r'(\d{2,5})-(\d{2,5})', f)]
    imglist = [f for f in os.listdir(fp) if splitext(f)[1].lower() in image_exts and not f in exception_list and f and re.search(r'(\d{2,5})-(\d{2,5})', f)]
    ## get only range numerals
    if not imglist:
        print(f'no image with range numerotations detected in folder, abort expand : {fp}')
        return 1

    imglist.sort()

    ## check if its a compressed sequence (not needed if already filterd or mixed numerotation)
    # for i in imglist:
    #     if not re.search(r'\d{2,5}-\d{2,5}\.?', i):
    #         print(f'image {i} has no compressed format name (range numerotation), skip {fp}')
    #         return 1
    
    padding = len(re.search(r'(\d{2,5})-(\d{2,5})\.?', imglist[0]).group(1))
    ct = 0
    for img in imglist:
        # if not re.search(r'(\d{2,5})-(\d{2,5})\.?', img):
        #     print(f'No range numerotation : {img}')
        #     continue
        base, ext = re.split(r'\d{2,5}-\d{2,5}', img)
        if not base:
            print('Base name of the sequence not found')
            return 1

        pic = join(fp, img)
        re_range = re.search(r'(\d{2,5})-(\d{2,5})\.?', img)
        start = int(re_range.group(1))
        end = int(re_range.group(2))
        if start > end:
            print(f'huge problem: end smaller than start -> {img}')
            return 1
        
        # newpath = join(fp, f'{base}{str(start).zfill(padding)}{ext}')
        newpath = join(fp, img.split('-')[0] + splitext(img)[-1])#get rid of end num (or use start)
        if start == end:
            print(f'{img} is a fix')
            os.rename(pic, newpath)
            continue
        
        # clone frame
        for i in range(start+1, end+1):
            copypath = join(fp, f'{base}{str(i).zfill(padding)}{ext}')
            if not exists(copypath):
                shutil.copy2(pic, copypath)
                ct+=1
            else:
                print(f'skip (already exists): {basename(copypath)}')
        
        os.rename(pic, newpath)
    
    if no_num:
        print(f'sequence has {len(no_num)} image with single numerotations')
    print(f'Sequence expanded : {ct} images created ({len(imglist) + len(no_num)} -> {len(imglist) + len(no_num) + ct} imgs)')
    

def sequence_from_img(src, limit, overwrite=False) -> int:
    '''take a src filepath and duplicate until limit number is reached'''
    if not exists(src):
        print('file not exists:', src)
        return

    loc = dirname(src)
    fname = basename(src)
    number = re.search(r'\d{4}', fname)
    if not number:
        print('no #### number found in file')
        return

    fill = len(number.group())
    num = int(number.group())

    ct = 0
    skipped = 0
    for i in range(num+1, limit+1):#depuis le suivant, jusqu'au dernier
        new = re.sub(r'\d{4}', str(i).zfill(fill), fname)
        dst = join(loc, new)
        if not overwrite and exists(dst):
            print('skipping existing', new)
            skipped += 1
            continue
        shutil.copyfile(src, dst)#accept only full fp and replace dest
        print('copy:', dst)
        ct += 1
    print('---')
    if ct:
        print(ct, 'generated')
    if skipped:
        print(skipped, 'skipped')
    return ct

def sequence_filler(src, limit=0, extend_last=0, logit=False):
    '''
    Fill gap in sequence with duplicated frames (in classic number style sequences)
    Can specify a limit and if last frame must be extended to a certain number
    (extend_last override limit parameter)
    '''
    if not exists(src):
        print('file not exists:', src)
        return

    renum = re.compile(r'\d{4}')
    fill = 4
    if isfile(src):
        loc = dirname(src)
    else:
        loc = src

    new_files = []
    gct = 0
    filelist = sorted([i for i in os.listdir(loc) if isfile(join(loc,i))])
    #TODO, valid list with number check, (maybe reject if there renum not passing on all)

    fcount = len(filelist)-1
    for i, f in enumerate(filelist):

        add=0
        # print(f)#print file name
        sf = join(loc, f)
        #get current elem num
        num = renum.search(f)
        if not num:
            print('skipped')
            continue
        num = int(num.group())
        if i == fcount:
            # print('End file')
            break

        #get next number
        next = filelist[i+1]
        nextnum = renum.search(next)
        if not nextnum:
            print(next, 'has no number')
            continue
        nextnum = int(nextnum.group())

        #error checks
        if nextnum < num:
            print('problem: current', num, '> next', nextnum)
            continue

        diff = nextnum - num
        if diff <= 0:
            print('problem, next has same number (or negative)!')

        #gap listing
        gap = [j for j in range(num, nextnum)]
        if len(gap) <= 1:
            continue#nogap
        else:
            gct +=1
            #slice first element of list (num)
            # print('gap:', gap[1:])
            for n in gap[1:]:
                nf = renum.sub(str(n).zfill(fill), f)
                df = join(loc, nf)
                if not exists(df):
                    new_files.append(df)
                    shutil.copyfile(sf, df)#copy2 ?
                    # print('copy to:', df)
                else:
                    print('Problem, detected has gap but already existed:', df)

    #treat last image if needed:
    #TODO:standalone this
    if extend_last and num:
        sequence_from_img(sf, num+extend_last)
    if limit and num:
        sequence_from_img(sf, limit)

    if not gct:
        print('No gap detected')
    else:
        print (gct, 'gap(s) filled')
    if new_files and logit:
        log = join(dirname(loc), basename(loc) + r'_log.txt')
        print('writing log:',log)
        with open(log, 'w') as fd:
            fd.write('\n'.join(new_files))

    print('Done')
    return

### available functions

# export_expo(seqs_parent_folder)# Export exposition of subfolders in txt files as index (easy tweak in function)

# compress_sequence(seq_filepath)# Remove redundant images and rename *key*frame with corresponding range

# compress_sequence(seq_filepath, rename=False)# Remove redundant images, Don't touch name and keep last image even if redundant

# sequence_gap_check(seq_filepath)# Check if there is gap in sequence with comprehensive prints

# expand_sequence(seq_filepath)# Fill gaps from compressed sequences with range in file names

# sequence_filler(seq_filepath)# Fill gaps in classic numbered sequences


### -- Batch launcher --

def compress_folders(folder_path, recursive=True, diff_percent=0.08):
    '''compress folder (default recursive) using compress_sequence func'''
    reports = []
    if recursive:
        for root, dirs, files in os.walk(folder_path):
            for d in dirs:
                fp = join(root,d)
                if [f for f in os.listdir(fp) if splitext(f)[1].lower() in image_exts and not f in exception_list]:
                    reports.append(compress_sequence(fp, diff_percent))
    else:
        ## Just use this directory
        compress_sequence(folder_path, diff_percent)
        ## use folder in directory
        # for d in os.scandir(folder_path):
        #     if d.is_dir():
        #         reports.append(compress_sequence(d.path))



    print('\n--Compression reports--')
    print('\n'.join(reports))

def expand_folders(folder_path, recursive=True):
    '''decompress folder (default recursive) using expand_sequence func'''
    reports = []
    if recursive:
        for root, dirs, files in os.walk(folder_path):
            for d in dirs:
                fp = join(root,d)
                if [f for f in os.listdir(fp) if splitext(f)[1].lower() in image_exts and not f in exception_list]:
                    expand_sequence(fp)# reports.append(
    else:
        ## Just use this directory
        expand_sequence(folder_path)
        ## use folder in directory
        # for d in os.scandir(folder_path):
        #     if d.is_dir():
        #         expand_sequence(d.path)# reports.append(

    # print('\n--Decompression reports--')
    # print('\n'.join(reports))
    print('-- Decompression Done--')


## ------
#  UI
## ------

class ListWidget(QWidget) :
    def __init__(self, label = ''):
        super().__init__()
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.setMinimumWidth(250)

        # Label
        self.label_layout = QHBoxLayout()
        self.label = QLabel(label)

        # Add
        self.btn_add_src = QPushButton('Add')
        self.btn_add_src.clicked.connect(self.browse)
        self.btn_add_src.setObjectName("btn")

        #DeepSkyBlue, DodgerBlue, Snow, Turquoise, Tomato 
        self.setStyleSheet("""
        QPushButton#btn {
            color: black;
            background-color: LightSlateGray ;
            selection-color: LightCyan;
            selection-background-color: yellow;
        }""")

        # remove
        self.btn_remove_src = QPushButton('Remove')
        self.btn_remove_src.setObjectName("btn")
        self.btn_remove_src.clicked.connect(self.del_input)

        self.label_layout.addWidget(self.label)
        self.label_layout.addStretch()
        self.label_layout.addWidget(self.btn_add_src)
        self.label_layout.addWidget(self.btn_remove_src)


        self.layout.addLayout(self.label_layout)


        self.list = QListWidget(self)
        self.list.setSelectionMode(QAbstractItemView.ExtendedSelection)


        self.setAcceptDrops(True)

        self.layout.addWidget(self.list)

        #self.dropped.connect(self.paths_drop)

    def browse(self) :
        dlg = QFileDialog(self)
        dlg.setFileMode(QFileDialog.Directory)

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            for i in filenames :
                self.add_input(i)


    def dragEnterEvent(self, event) :
        event.accept()

    def dropEvent(self, event):
        # All
        # files = ([u.toLocalFile()  for u in event.mimeData().urls()])

        # Folder
        files = ([u.toLocalFile()  for u in event.mimeData().urls() if isdir( u.toLocalFile() )])

        #files = [unicode(u.toLocalFile()) for u in event.mimeData().urls()]
        for f in files:
            self.add_input(f)

    def add_input(self, text) :
        item = QListWidgetItem(text, self.list)
        item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
        # item.setCheckState(Qt.Unchecked)# unchecked on add
        item.setCheckState(Qt.Checked)# checked on add

        print('Add Input')

    def del_input(self):
        # deleted = self.list.takeItem(self.list.currentRow())

        rows = [i for i in range(0,self.list.count()) if self.list.item(i).isSelected()]
        print(rows)
        for i in rows[::-1]:
            deleted = self.list.takeItem(i)
            print(deleted.text())

        print('delete')


    def paths_drop(self, paths) :
        print('drop')

class SrcLayout(QHBoxLayout) :
    def __init__(self, layout):
        super().__init__()

        ## Paths list 
        self.path_layout = QVBoxLayout()
        self.wlist = ListWidget('Sources')
        self.path_layout.addWidget(self.wlist)
        self.addLayout(self.path_layout)
        
        ## Settings
        self.settings_layout = QVBoxLayout()


        # btn = QPushButton('Settings')
        # self.settings_layout.addWidget(btn)

        # self.settings_layout.addItem(QSpacerItem(0, 35))
        ## content margin order : ←↑→↓
        self.settings_layout.setContentsMargins(5,37,5,7)
        self.settings_layout.setSpacing(20)

        self.cbox_expand = QCheckBox('Expand (recreate sequence)')

        # self.settings_layout.addWidget( QLabel('Expand instead of compress') )
        self.settings_layout.addWidget(self.cbox_expand)
        
        self.cbox_recursive = QCheckBox('Recursive (enter subfolder)')
        # self.label = QLabel('Affect subfolder')
        # self.settings_layout.addWidget(self.label)
        self.settings_layout.addWidget(self.cbox_recursive)

        ## Diff tolerance, the percentage of pixel difference to consider image are the same (lower = more sensible)
        self.diff_layout = QHBoxLayout()
        
        self.label = QLabel('Diff percentage:')
        self.diff_layout.addWidget(self.label)
        self.ebox_tolerance = QLineEdit() # TODO replace by a QDoubleSpinBox for a float 
        self.ebox_tolerance.maximumWidth()
        # self.ebox_tolerance.setTextMargins(5,5,5,5)


        self.ebox_tolerance.setPlaceholderText('0.08')
        self.diff_layout.addWidget(self.ebox_tolerance)
        # self.diff_layout.setContentsMargins(5,5,5,5)
        
        # self.diff_layout.addItem(QSpacerItem(0, 35, hPolicy = QSizePolicy.Preferred))# Expand ! want to shrink
        self.settings_layout.addLayout(self.diff_layout)

        
        # self.settings_layout.addWidget(QLabel('infos: Percentage of pixels variation to consider different'))
        # self.settings_layout.addWidget(QLabel('(lower = more sensitive)'))

        spacer = QSpacerItem(160, 0, hPolicy = QSizePolicy.Preferred)
        self.settings_layout.addItem(spacer)

        self.settings_layout.addStretch()


        # -> maybe add a QVbox layout and use (QSizePolicy.Preferred, QSizePolicy.Expanding) or just (vPolicy = QSizePolicy.Expanding)
        ## Run
        self.btn_run = QPushButton('Run')
        # self.btn_run.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)# expand vertically (if there is a stretch)

        ### self.btn_run.setObjectName("btn")
        self.settings_layout.addWidget(self.btn_run)
        self.btn_run.clicked.connect(self.run)


        self.addLayout(self.settings_layout)
        
        layout.addLayout(self)

    def run(self):
        # items > 'background', 'backgroundColor', 'checkState', 'clone', 'data', 'flags', 'font', 'foreground', 'icon', 
        # 'isHidden', 'isSelected', 'listWidget', 'read', 'setBackground', 'setBackgroundColor', 'setCheckState', 
        # 'setData', 'setFlags', 'setFont', 'setForeground', 'setHidden', 'setIcon', 'setSelected', 'setSizeHint', 
        # 'setStatusTip', 'setText', 'setTextAlignment', 'setTextColor', 'setToolTip', 'setWhatsThis', 'sizeHint', 
        # 'statusTip', 'text', 'textAlignment', 'textColor', 'toolTip', 'type', 'whatsThis', 'write'

        diff_percent = self.ebox_tolerance.text()
        if not diff_percent:
            diff_percent = 0.08
        else:
            try:
                diff_percent = float(diff_percent)
            except:
                print('could not parse diff_percent')
                return
        
        print('differencial percentage: ', diff_percent)

        # i.checkState() evaluate true/false (use bool(i.checkState()) to get real boolean value)
        items = [self.wlist.list.item(i) for i in range(self.wlist.list.count()) if self.wlist.list.item(i).checkState()]# isSelected()
        txt_recursive = ' (recursive)' if self.cbox_recursive.isChecked() else ''

        for i in items:
            if self.cbox_expand.isChecked():
                print(f'Expand{txt_recursive}: {i.text()}')
                expand_folders(i.text(), self.cbox_recursive.isChecked())
            else:
                print(f'Compress{txt_recursive}: {i.text()}')
                compress_folders(i.text(), self.cbox_recursive.isChecked(), diff_percent)

class MainWindow(QWidget) :
    def __init__(self):
        super().__init__()
        # QCoreApplication.setOrganizationName('settings')
        # QCoreApplication.setApplicationName('Sequence redundancy handler')
        # self.settings = QSettings()
        self.setWindowTitle('Image sequences redundancy handler')# redundancy handler
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.src_layout = SrcLayout(self.layout)


### how to store settings

# self.settings = QSettings()
# QSettings() sans arguments ça te retourne les settings de ton application
# QSettings().setValue('pourcentage', 0.5 ) store settings 
# 
# value = QSettings().value('pourcentage')
# self.widget_pourcentage.setValue(value)

if __name__ == '__main__':
    app = QApplication(sys.argv)

    window = MainWindow()
    window.resize(1280, 720)

    window.show()
    app.exec_()

