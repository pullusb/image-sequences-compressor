# Image sequences compressor

Image sequences redundancy handler with GUI

Allow to compress / fill / expand image sequences

### [Download single python file](https://gitlab.com/pullusb/image-sequences-compressor/-/raw/master/img_seq_compressor.py) (Use right clic > save target as)

Requirement :

- [Python](https://www.python.org/) >= 3.6
- [PySide2](https://pypi.org/project/PySide2/) (Launch in a terminal: `pip install PySide2`, the program will try to auto install this lib)
- [Image magick](https://imagemagick.org/script/download.php) (Folder IMagick executables need to be added in your PATH to access `compare` bin)

Tick `expand` if you want to "decompress" (recreate full sequence).

Thanks to [Christophe Seux](https://gitlab.com/users/ChristopheSeux/projects) for his help with QT.

## Todo:

- Add no-range mode
- Try to make a build integrating imagick's compare bin.

## CHANGELOG


0.2.0:

- change compare metric method from AE (Absolute pixel error) to RMSE (root mean square error) better handling of the overall diff.
- print detected RMSE
- add a field to change sensitivity percentage value (default is 0.08%)

0.1.2:

- Ready to pyinstall (need image magick's executable 'compare.exe' at the root)

0.1.1:

- added: auto-install pyside2 module via pip if impossible to import


<!-- https://keepachangelog.com/en/1.0.0/
https://www.conventionalcommits.org/en/v1.0.0 -->